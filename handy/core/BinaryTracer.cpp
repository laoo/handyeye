#include "pch.hpp"
#include "BinaryTracer.hpp"
#include "Eye.hpp"

BinaryTracer::BinaryTracer( std::filesystem::path const & outputPath ) : mEye{ std::make_shared<Eye>() }, mPrefetch{}
{
}

BinaryTracer & BinaryTracer::trace( CpuCtx const * ctx )
{
  mCtx = ctx;
	mIdx = 0;
	mOpcodesToFetch = 0;
  return *this;
}

void BinaryTracer::prefetchb()
{
  mPrefetch = 1;
}

void BinaryTracer::prefetchw()
{
  mPrefetch = 2;
}

uint8_t BinaryTracer::fetchb( uint16_t adr, uint8_t value )
{
  mEye->fetch( adr );
  return value;
}


uint16_t BinaryTracer::fetchw( uint16_t adr, uint16_t value )
{
  mEye->fetch( adr );
  mEye->fetch( adr + 1 );
  return value;
}

uint8_t BinaryTracer::readb( uint16_t adr, uint8_t value )
{
  if ( mPrefetch > 0 )
  {
    mPrefetch -= 1;
    return fetchb( adr, value );
  }
  else
  {
    mEye->readMikey( adr );
    return value;
  }
}

uint16_t BinaryTracer::readw( uint16_t adr, uint16_t value )
{
  if ( mPrefetch > 0 )
  {
    mPrefetch -= 2;
    return fetchw( adr, value );
  }
  else
  {
    mEye->readMikey( adr );
    mEye->readMikey( adr + 1 );
    return value;
  }

}

void BinaryTracer::writeb( uint16_t adr, uint8_t value )
{
  mEye->writeMikey( adr, value );
}

uint8_t BinaryTracer::romReadb( uint16_t adr, uint8_t value )
{
  if ( mPrefetch > 0 )
  {
    mPrefetch -= 1;
    return value;
  }
  else
    return value;
}

uint8_t BinaryTracer::suzyReadb( uint16_t adr, uint8_t value )
{
  mEye->readSuzy( adr );
  return value;
}

uint16_t BinaryTracer::suzyReadw( uint16_t adr, uint16_t value )
{
  mEye->readSuzy( adr );
  mEye->readSuzy( adr + 1 );
  return value;
}

void BinaryTracer::suzyWriteb( uint16_t adr, uint8_t value )
{
  mEye->writeSuzy( adr, value );
}

void BinaryTracer::tick()
{
  mEye->tick();
}


