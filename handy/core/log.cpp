#include "pch.hpp"
#include "log.h"
#include <functional>
#include <chrono>
#include <algorithm>


static constexpr size_t LOG_BUFFER_SIZE = 16 * 1024 * 1024;

LogSingleton::LogSingleton() : mLog{ LOG_BUFFER_SIZE }, mLogWrite{ 0 }, mLogRead{ 0 }, mCpuWrite{ 0 }, mCpuRead{ 0 }, mThread{ std::bind( &LogSingleton::logger, this ) }, mWork{ true },
  mLogFout{}, mWillLog{ false }
{
}

char * LogSingleton::CyclicBuffer::alloc( int size )
{
  std::lock_guard<std::mutex> g{ mMutex };

  if ( writeCursor + size >= buffer.size() - 1 )
  {
    endCursor = writeCursor;
    writeCursor = 0;
  }

  auto result = buffer.data() + writeCursor;
  writeCursor += size;
  return result;
}

void LogSingleton::CyclicBuffer::writeText( std::ofstream & out )
{
  std::lock_guard<std::mutex> g{ mMutex };

  if ( readCursor < writeCursor )
  {
    auto it = std::find( buffer.data() + readCursor, buffer.data() + writeCursor, 0 );
    auto size = it - ( buffer.data() + readCursor );
    out.write( buffer.data() + readCursor, size );
    readCursor += size;
  }
  else
  {
    auto it = std::find( buffer.data() + readCursor, buffer.data() + endCursor, 0 );
    if ( auto size = it - ( buffer.data() + endCursor ) )
    {
      out.write( buffer.data() + readCursor, size );
    }
    readCursor = 0;
  }
}

void LogSingleton::logger()
{
  using namespace std::chrono_literals;

  while ( mWork.load() )
  {
    if ( mLog )
    {
      mLog.writeText( mLogFout );
    }
    else
    {
      std::this_thread::sleep_for( 1ms );
    }
  }
}

LogSingleton::~LogSingleton()
{
  mWork.store( false );
  mThread.join();
}

char * LogSingleton::logalloc( int size )
{
  return mLog.alloc( size );
}

bool LogSingleton::willLog( uint32_t module, int priority )
{
  return mWillLog;
}

void LogSingleton::setFout( std::filesystem::path const & path )
{
  mLogFout = std::ofstream{ path / std::string{ "handylog.log" } };
}

void LogSingleton::enable( bool condition )
{
  if ( condition )
  {
    mWillLog = mLogFout.good();
  }
  else
  {
    mWillLog = false;
  }
}

LogSingleton & LogSingleton::instance()
{
  static LogSingleton instance{};
  return instance;
}
