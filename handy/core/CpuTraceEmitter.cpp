#include "pch.hpp"
#include "CpuTraceEmitter.hpp"
#include <functional>
#include <climits>

static const size_t NODE_SIZE = 65536;

CpuTraceEmitter::CpuTraceEmitter( std::filesystem::path const& outputPath ) :
  mFirstNode{}, mLastNode{}, mThread{}, mWork{ true }, mLogFout{}, mNewMutex{}
{
  mFirstNode = mLastNode = std::make_shared<Node>();
  mThread = std::thread{ std::bind( &CpuTraceEmitter::worker, this ) };
}

CpuTraceEmitter::~CpuTraceEmitter()
{
  mWork.store( false );
  mThread.join();
}

CpuTraceEmitter::Mem CpuTraceEmitter::alloc()
{
  if ( mLastNode->size.load() + sizeof( TracerCell ) >= NODE_SIZE )
  {
    std::scoped_lock<std::mutex> lock{ mNewMutex };

    mLastNode->finish.store( true );
    if ( !mLastNode->next )
      mLastNode->next = std::make_shared<Node>();
    mLastNode = mLastNode->next;
  }


  return Mem{ mLastNode, mLastNode->mem.get() + mLastNode->size };
}

void CpuTraceEmitter::worker()
{
  using namespace std::chrono_literals;
  for ( ;; )
  {
    while ( mFirstNode->readOffset < mFirstNode->size )
    {
      process( mFirstNode->mem.get() + mFirstNode->readOffset++ );
    }

    if ( mFirstNode->finish.load() )
    {
      std::scoped_lock<std::mutex> lock{ mNewMutex };

      auto empty = mFirstNode;
      mFirstNode = mFirstNode->next;
      empty->next.reset();
      empty->readOffset = 0;
      empty->size.store( 0 );
      empty->finish.store( false );
      mLastNode->next = empty;
      continue;
    }
    else
    {
      if ( mWork.load() )
      {
        std::this_thread::sleep_for( 100ms );
      }
      else
      {
        break;
      }
    }
  }
}

void CpuTraceEmitter::process( TracerCell const* ptr )
{
  mLogFout << std::dec << std::setfill( ' ' ) << std::setw( 10 ) << ptr->cycle << " ";
  mLogFout << std::hex << std::setw( 4 ) << std::setfill( '0' ) << (uint32_t)ptr->pc << " ";
  mLogFout << "a=" << std::hex << std::setw( 2 ) << std::setfill( '0' ) << (uint32_t)ptr->a << " x=" << std::setw( 2 ) << (uint32_t)ptr->x << " y=" << std::setw( 2 ) << (uint32_t)ptr->y << " s=" << std::setw( 2 ) << (uint32_t)ptr->s << " p(";
  mLogFout << ( ptr->p & 0b10000000 ? "N" : " " );
  mLogFout << ( ptr->p & 0b01000000 ? "V" : " " );
  mLogFout << ( ptr->p & 0b00010000 ? "B" : " " );
  mLogFout << ( ptr->p & 0b00001000 ? "D" : " " );
  mLogFout << ( ptr->p & 0b00000100 ? "I" : " " );
  mLogFout << ( ptr->p & 0b00000010 ? "Z" : " " );
  mLogFout << ( ptr->p & 0b00000001 ? "C" : " " ) << ") ";
  for ( int i = 0; i < 8; ++i )
  {
    mLogFout.put( isprint( (int)ptr->operandText[i] ) ? ptr->operandText[i] : ' ' );
  }
  mLogFout.write( ptr->text, ptr->textSize );
  mLogFout << std::endl;
}

CpuTraceEmitter::Node::Node() : mem{ std::shared_ptr<TracerCell[]>( new TracerCell[NODE_SIZE] ) }, next{}, size{}, readOffset{}, finish{}
{
}

CpuTraceEmitter::Mem::Mem( std::shared_ptr<Node> node, TracerCell * ptr ) : node{ node }, cell{ ptr }
{
}

CpuTraceEmitter::Mem::~Mem()
{
  node->size.fetch_add( 1 );
}
