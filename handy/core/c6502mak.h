//
// Copyright (c) 2004 K. Wilkins
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not
//    be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//

//////////////////////////////////////////////////////////////////////////////
//                       Handy - An Atari Lynx Emulator                     //
//                          Copyright (c) 1996,1997                         //
//                                 K. Wilkins                               //
//////////////////////////////////////////////////////////////////////////////
// 65C02 Macro definitions                                                  //
//////////////////////////////////////////////////////////////////////////////
//                                                                          //
// This file contains all of the required address mode and operand          //
// macro definitions for the 65C02 emulation                                //
//                                                                          //
//    K. Wilkins                                                            //
// August 1997                                                              //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////
// Revision History:                                                        //
// -----------------                                                        //
//                                                                          //
// 01Aug1997 KW Document header added & class documented.                   //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

//
// Addressing mode decoding
//

#define	xIMMEDIATE()						{tracer->prefetchb();mCtx.operand=mCtx.pc;mCtx.pc++;}
#define	xABSOLUTE()							{mCtx.operand=CPU_FETCHW(mCtx.pc);mCtx.pc+=2;}
#define xZEROPAGE()							{mCtx.operand=CPU_FETCH(mCtx.pc);mCtx.pc++;}
#define xZEROPAGE_X()						{mCtx.operand=CPU_FETCH(mCtx.pc)+mCtx.x;mCtx.pc++;mCtx.operand&=0xff;}
#define xZEROPAGE_Y()						{mCtx.operand=CPU_FETCH(mCtx.pc)+mCtx.y;mCtx.pc++;mCtx.operand&=0xff;}
#define xABSOLUTE_X()						{mCtx.operand=CPU_FETCHW(mCtx.pc);mCtx.pc+=2;mCtx.operand+=mCtx.x;mCtx.operand&=0xffff;}
#define	xABSOLUTE_Y()						{mCtx.operand=CPU_FETCHW(mCtx.pc);mCtx.pc+=2;mCtx.operand+=mCtx.y;mCtx.operand&=0xffff;}
#define xINDIRECT_ABSOLUTE_X()	{mCtx.operand=CPU_FETCHW(mCtx.pc);mCtx.pc+=2;mCtx.operand+=mCtx.x;mCtx.operand&=0xffff;mCtx.operand=CPU_PEEKW(mCtx.operand);}
#define xINDIRECT_X()						{mCtx.operand=CPU_FETCH(mCtx.pc);mCtx.pc++;mCtx.operand=mCtx.operand+mCtx.x;mCtx.operand&=0x00ff;mCtx.operand=CPU_PEEKW(mCtx.operand);}
#define xINDIRECT_Y()						{mCtx.operand=CPU_FETCH(mCtx.pc);mCtx.pc++;mCtx.operand=CPU_PEEKW(mCtx.operand);mCtx.operand=mCtx.operand+mCtx.y;mCtx.operand&=0xffff;}
#define xINDIRECT_ABSOLUTE()		{mCtx.operand=CPU_FETCHW(mCtx.pc);mCtx.pc+=2;mCtx.operand=CPU_PEEKW(mCtx.operand);}
#define xINDIRECT()							{mCtx.operand=CPU_FETCH(mCtx.pc);mCtx.pc++;mCtx.operand=CPU_PEEKW(mCtx.operand);}

//
// Helper Macros
//
//#define SET_Z(m)				{ mCtx.z=(m)?false:true; }
//#define SET_N(m)				{ mCtx.n=(m&0x80)?true:false; }
//#define SET_NZ(m)				SET_Z(m) SET_N(m)
#define SET_Z(m)				{ mCtx.z=!(m); }
#define SET_N(m)				{ mCtx.n=(m)&0x80; }
#define SET_NZ(m)				{ mCtx.z=!(m); mCtx.n=(m)&0x80; }
#define PULL(m)					{ mCtx.sp++; mCtx.sp&=0xff; m=CPU_PEEK(mCtx.sp+0x0100); }
#define PUSH(m)					{ CPU_POKE(0x0100+mCtx.sp,m); mCtx.sp--; mCtx.sp&=0xff; }
//
// Opcode execution
//

#define xADC()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	if(mCtx.d)\
	{\
		int c = mCtx.c?1:0;\
		int lo = (mCtx.a & 0x0f) + (value & 0x0f) + c;\
		int hi = (mCtx.a & 0xf0) + (value & 0xf0);\
		mCtx.v=0;\
		mCtx.c=0;\
		if (lo > 0x09)\
	    {\
		    hi += 0x10;\
			lo += 0x06;\
	    }\
		if (~(mCtx.a^value) & (mCtx.a^hi) & 0x80) mCtx.v=1;\
	    if (hi > 0x90) hi += 0x60;\
		if (hi & 0xff00) mCtx.c=1;\
		mCtx.a = (lo & 0x0f) + (hi & 0xf0);\
	}\
	else\
	{\
		int c = mCtx.c?1:0;\
		int sum = mCtx.a + value + c;\
	    mCtx.v=0;\
		mCtx.c=0;\
	    if (~(mCtx.a^value) & (mCtx.a^sum) & 0x80) mCtx.v=1;\
		if (sum & 0xff00) mCtx.c=1;\
	    mCtx.a = (UBYTE) sum;\
	}\
	SET_NZ(mCtx.a)\
 }

#define xAND()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.a&=value;\
	SET_NZ(mCtx.a);\
}

#define xASL()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	mCtx.c=value&0x80;\
	value<<=1;\
	value&=0xff;\
	SET_NZ(value);\
	CPU_POKE(mCtx.operand,value);\
}

#define xASLA()\
{\
	mCtx.c=mCtx.a&0x80;\
	mCtx.a<<=1;\
	mCtx.a&=0xff;\
	SET_NZ(mCtx.a);\
}

#define xBCC()\
{\
	if(!mCtx.c)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
    mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBCS()\
{\
	if(mCtx.c)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBEQ()\
{\
	if(mCtx.z)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

// This version of bit, not setting N and V status flags in immediate, seems to be correct.
// The same behaviour is reported on the 65C02 used in old Apple computers, at least.
// (From a pragmCtx.Atic sense, using the normCtx.Al version of bit for immediate
// mode breaks the title screen of "California Games" in a subtle way.)
#define	xBIT()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	SET_Z(mCtx.a&value);\
	if(mCtx.opcode!=0x89)\
	{\
		mCtx.n=value&0x80;\
		mCtx.v=value&0x40;\
	}\
}

#define	xBMI()\
{\
	if(mCtx.n)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBNE()\
{\
	if(!mCtx.z)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBPL()\
{\
	if(!mCtx.n)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBRA()\
{\
	int offset=(signed char)CPU_FETCH(mCtx.pc);\
	mCtx.pc++;\
	mCtx.pc+=offset;\
	mCtx.pc&=0xffff;\
}

#define	xBRK()\
{\
	mCtx.pc++;\
    PUSH(mCtx.pc>>8);\
	PUSH(mCtx.pc&0xff);\
	PUSH(mCtx.PS()|0x10);\
\
	mCtx.d=FALSE;\
	mCtx.i=TRUE;\
\
	mCtx.pc=CPU_PEEKW(IRQ_VECTOR);\
}
// KW 4/11/98 B flag needed to be set IN the stack status word = 0x10.

#define	xBVC()\
{\
	if(!mCtx.v)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xBVS()\
{\
	if(mCtx.v)\
	{\
		int offset=(signed char)CPU_FETCH(mCtx.pc);\
		mCtx.pc++;\
		mCtx.pc+=offset;\
		mCtx.pc&=0xffff;\
	}\
	else\
	{\
		mCtx.pc++;\
		mCtx.pc&=0xffff;\
	}\
}

#define	xCLC()\
{\
	mCtx.c=FALSE;\
}

#define	xCLD()\
{\
	mCtx.d=FALSE;\
}

#define	xCLI()\
{\
	mCtx.i=FALSE;\
}

#define	xCLV()\
{\
	mCtx.v=FALSE;\
}

#define	xCMP()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.c=0;\
	if (mCtx.a >= value) mCtx.c=1;\
	SET_NZ((UBYTE)(mCtx.a - value))\
}

#define	xCPX()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.c=0;\
	if (mCtx.x >= value) mCtx.c=1;\
	SET_NZ((UBYTE)(mCtx.x - value))\
}

#define	xCPY()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.c=0;\
	if (mCtx.y >= value) mCtx.c=1;\
	SET_NZ((UBYTE)(mCtx.y - value))\
}

#define	xDEC()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	value-=1;\
	value&=0xff;\
	CPU_POKE(mCtx.operand,value);\
	SET_NZ(value);\
}

#define	xDECA()\
{\
	mCtx.a--;\
	mCtx.a&=0xff;\
	SET_NZ(mCtx.a);\
}

#define	xDEX()\
{\
	mCtx.x--;\
	mCtx.x&=0xff;\
	SET_NZ(mCtx.x);\
}

#define	xDEY()\
{\
	mCtx.y--;\
	mCtx.y&=0xff;\
	SET_NZ(mCtx.y);\
}

#define	xEOR()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.a^=value;\
	SET_NZ(mCtx.a);\
}

#define	xINC()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	value+=1;\
	value&=0xff;\
	CPU_POKE(mCtx.operand,value);\
	SET_NZ(value);\
}

#define	xINCA()\
{\
	mCtx.a++;\
	mCtx.a&=0xff;\
	SET_NZ(mCtx.a);\
}

#define	xINX()\
{\
	mCtx.x++;\
	mCtx.x&=0xff;\
	SET_NZ(mCtx.x);\
}

#define	xINY()\
{\
	mCtx.y++;\
	mCtx.y&=0xff;\
	SET_NZ(mCtx.y);\
}

#define	xJMP()\
{\
	mCtx.pc=mCtx.operand;\
}

#define	xJSR()\
{\
	PUSH((mCtx.pc-1)>>8);\
	PUSH((mCtx.pc-1)&0xff);\
	mCtx.pc=mCtx.operand;\
}

#define	xLDA()\
{\
	mCtx.a=CPU_PEEK(mCtx.operand);\
	SET_NZ(mCtx.a);\
}

#define	xLDX()\
{\
	mCtx.x=CPU_PEEK(mCtx.operand);\
	SET_NZ(mCtx.x);\
}

#define	xLDY()\
{\
	mCtx.y=CPU_PEEK(mCtx.operand);\
	SET_NZ(mCtx.y);\
}

#define	xLSR()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	mCtx.c=value&0x01;\
	value=(value>>1)&0x7f;\
	CPU_POKE(mCtx.operand,value);\
	SET_NZ(value);\
}

#define	xLSRA()\
{\
	mCtx.c=mCtx.a&0x01;\
	mCtx.a=(mCtx.a>>1)&0x7f;\
	SET_NZ(mCtx.a);\
}

#define	xNOP()\
{\
}

#define	xORA()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	mCtx.a|=value;\
	SET_NZ(mCtx.a);\
}

#define	xPHA()\
{\
	 PUSH(mCtx.a);\
}

#define	xPHP()\
{\
	 PUSH(mCtx.PS());\
}

#define	xPHX()\
{\
	 PUSH(mCtx.x);\
}

#define	xPHY()\
{\
	 PUSH(mCtx.y);\
}

#define	xPLA()\
{\
	PULL(mCtx.a);\
	SET_NZ(mCtx.a);\
}

#define	xPLP()\
{\
	int P;\
	PULL(P); \
	mCtx.PS(P);\
}

#define	xPLX()\
{\
	PULL(mCtx.x); \
	SET_NZ(mCtx.x);\
}

#define	xPLY()\
{\
	PULL(mCtx.y); \
	SET_NZ(mCtx.y);\
}

#define	xROL()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	int oldC=mCtx.c;\
	mCtx.c=value&0x80;\
	value=(value<<1)|(oldC?1:0);\
	value&=0xff;\
	CPU_POKE(mCtx.operand,value);\
	SET_NZ(value);\
}

#define	xROLA()\
{\
	int oldC=mCtx.c;\
	mCtx.c=mCtx.a&0x80;\
	mCtx.a=(mCtx.a<<1)|(oldC?1:0);\
	mCtx.a&=0xff;\
	SET_NZ(mCtx.a);\
}

#define	xROR()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	int oldC=mCtx.c;\
	mCtx.c=value&0x01;\
	value=((value>>1)&0x7f)|(oldC?0x80:0x00);\
	value&=0xff;\
	CPU_POKE(mCtx.operand,value);\
	SET_NZ(value);\
}

#define	xRORA()\
{\
	int oldC=mCtx.c;\
	mCtx.c=mCtx.a&0x01;\
	mCtx.a=((mCtx.a>>1)&0x7f)|(oldC?0x80:0x00);\
	mCtx.a&=0xff;\
	SET_NZ(mCtx.a);\
}

#define	xRTI()\
{\
	int p;\
	PULL(p);\
	mCtx.PS(p);\
	PULL(mCtx.pc);\
  int tmp;\
	PULL(tmp);\
	mCtx.pc|=tmp<<8;\
}

#define	xRTS()\
{\
	int tmp;\
	PULL(mCtx.pc);\
	PULL(tmp);\
	mCtx.pc|=tmp<<8;\
	mCtx.pc++;\
}

#define	xSBC()\
{\
	int const value=CPU_PEEK(mCtx.operand);\
	if (mCtx.d)\
	{\
		int c = mCtx.c?0:1;\
		int sum = mCtx.a - value - c;\
		int lo = (mCtx.a & 0x0f) - (value & 0x0f) - c;\
		int hi = (mCtx.a & 0xf0) - (value & 0xf0);\
	    mCtx.v=0;\
		mCtx.c=0;\
	    if ((mCtx.a^value) & (mCtx.a^sum) & 0x80) mCtx.v=1;\
	    if (lo & 0xf0) lo -= 6;\
	    if (lo & 0x80) hi -= 0x10;\
	    if (hi & 0x0f00) hi -= 0x60;\
	    if ((sum & 0xff00) == 0) mCtx.c=1;\
	    mCtx.a = (lo & 0x0f) + (hi & 0xf0);\
	}\
	else\
	{\
		int c = mCtx.c?0:1;\
		int sum = mCtx.a - value - c;\
	    mCtx.v=0;\
		mCtx.c=0;\
	    if ((mCtx.a^value) & (mCtx.a^sum) & 0x80) mCtx.v=1;\
	    if ((sum & 0xff00) == 0) mCtx.c=1;\
	    mCtx.a = (UBYTE) sum;\
	}\
	SET_NZ(mCtx.a)\
}

#define	xSEC()\
{\
	mCtx.c=true;\
}

#define	xSED()\
{\
	mCtx.d=true;\
}

#define	xSEI()\
{\
	mCtx.i=true;\
}

#define	xSTA()\
{\
	uint8_t const value=mCtx.a;\
	CPU_POKE(mCtx.operand,value);\
}

#define	xSTP()\
{\
	gSystemCPUSleep=TRUE;\
}

#define	xSTX()\
{\
	uint8_t const value=mCtx.x;\
	CPU_POKE(mCtx.operand,value);\
}

#define	xSTY()\
{\
	uint8_t const value=mCtx.y;\
	CPU_POKE(mCtx.operand,value);\
}

#define	xSTZ()\
{\
	uint8_t const value=0;\
	CPU_POKE(mCtx.operand,value);\
}

#define	xTAX()\
{\
	mCtx.x=mCtx.a;\
	SET_NZ(mCtx.x);\
}

#define	xTAY()\
{\
	mCtx.y=mCtx.a;\
	SET_NZ(mCtx.y);\
}

#define	xTRB()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	SET_Z(mCtx.a&value);\
	value=value&(mCtx.a^0xff);\
	CPU_POKE(mCtx.operand,value);\
}

#define	xTSB()\
{\
	int value=CPU_PEEK(mCtx.operand);\
	SET_Z(mCtx.a&value);\
	value=value|mCtx.a;\
	CPU_POKE(mCtx.operand,value);\
}

#define	xTSX()\
{\
	mCtx.x=mCtx.sp;\
	SET_NZ(mCtx.x);\
}

#define	xTXA()\
{\
	mCtx.a=mCtx.x;\
	SET_NZ(mCtx.a);\
}

#define	xTXS()\
{\
	mCtx.sp=mCtx.x;\
}

#define	xTYA()\
{\
	mCtx.a=mCtx.y;\
	SET_NZ(mCtx.a);\
}

#define	xWAI()\
{\
	gSystemCPUSleep=TRUE;\
}

