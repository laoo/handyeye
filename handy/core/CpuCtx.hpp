#pragma once

#include <cstdint>

struct CpuCtx
{
  int a;
  int x;
  int y;
  int sp;
  int opcode;
  int operand;
  int pc;

  int n;
  int v;
  int b;
  int d;
  int i;
  int z;
  int c;
  int IRQActive;

  // Answers value of the Processor Status register
  int PS() const
  {
    uint8_t ps = 0x20;
    if ( n ) ps |= 0x80;
    if ( v ) ps |= 0x40;
    if ( b ) ps |= 0x10;
    if ( d ) ps |= 0x08;
    if ( i ) ps |= 0x04;
    if ( z ) ps |= 0x02;
    if ( c ) ps |= 0x01;
    return ps;
  }

  // Change the processor flags to correspond to the given value
  void PS( int ps )
  {
    n = ps & 0x80;
    v = ps & 0x40;
    b = ps & 0x10;
    d = ps & 0x08;
    i = ps & 0x04;
    z = ps & 0x02;
    c = ps & 0x01;
  }

};
