#pragma once
#include <fstream>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>

class LogSingleton
{
public:
  LogSingleton( LogSingleton const& ) = delete;
  void operator=( LogSingleton const& ) = delete;
  ~LogSingleton();

  char * logalloc( int size );
  bool willLog( uint32_t module, int priority );
  void setFout( std::filesystem::path const& path );
  void enable( bool condition );

  static LogSingleton& instance();

private:
  LogSingleton();
  void logger();

  class CyclicBuffer
  {
    std::vector<char> buffer;
    uint32_t readCursor;
    uint32_t writeCursor;
    uint32_t endCursor;

    std::mutex mMutex;

  public:
    CyclicBuffer( size_t size ) : buffer( size, 0 ), readCursor{}, writeCursor{}, endCursor{}, mMutex{} {}

    explicit operator bool() const
    {
      return readCursor != writeCursor;
    }

    char * alloc( int size );

    template<typename T>
    void push( T v )
    {
      std::lock_guard<std::mutex> g{ mMutex };

      if ( writeCursor + sizeof(T) >= buffer.size() - 1 )
      {
        endCursor = writeCursor + 1;
        writeCursor = 0;
      }

      *reinterpret_cast<T*>( &buffer[writeCursor] ) = v;
      writeCursor += sizeof(T);
    }

    void writeText( std::ofstream & out );
  };

private:
  CyclicBuffer mLog;
  size_t mLogWrite;
  size_t mLogRead;
  size_t mCpuWrite;
  size_t mCpuRead;
  std::thread mThread;
  std::atomic_bool mWork;
  std::ofstream mLogFout;
  bool mWillLog;
};

enum Module
{
  MODULE_SUZY = 0,
  MODULE_MIKEY = 1,
  MODULET_ATTRIBUTE_LOADER = 2
};


class LogFormatter
{
  bool mWillLog;
public:
  LogFormatter( Module module, int priority ) : mWillLog{ LogSingleton::instance().willLog( module, priority ) }
  {
  }

  explicit operator bool() const
  {
    return mWillLog;
  }

  template<typename... Args>
  void operator()( char const * aFormat, Args... args )
  {
    auto size = snprintf( nullptr, 0, aFormat, std::forward<Args>( args )... );
    char * buf = LogSingleton::instance().logalloc( size );
    sprintf( buf, aFormat, std::forward<Args>( args )... );
  }
};


#define LOG_ERROR( aFormat, ... ) do { if ( auto log = LogFormatter( LOG_MODULE, 3 ) ) log( aFormat, ##__VA_ARGS__ ); } while( false )
#define LOG_INFO( aFormat, ... ) do { if ( auto log = LogFormatter( LOG_MODULE, 2 ) ) log( aFormat, ##__VA_ARGS__ ); } while( false )
#define LOG_DEBUG( aFormat, ... ) do { if ( auto log = LogFormatter( LOG_MODULE, 1 ) ) log( aFormat, ##__VA_ARGS__ ); } while( false )
#define LOG_TRACE( aFormat, ... ) do { if ( auto log = LogFormatter( LOG_MODULE, 0 ) ) log( aFormat, ##__VA_ARGS__ ); } while( false )


#define PUSH_CPU_OPCODE( CYCLE, PC, OPCODE ) LogSingleton::instance().pushCPUOpcode( CYCLE, PC, OPCODE )
#define PUSH_CPU_OPERAND( OPERAND ) LogSingleton::instance().pushCPUOperand( OPERAND )
