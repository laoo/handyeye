#pragma once

#include <thread>
#include <atomic>
#include <mutex>
#include <list>
#include <vector>
#include <memory>
#include <fstream>
#include <filesystem>
#include <mutex>
#include "CpuTracer.h"


struct TracerCell
{
  char const* text;
  uint32_t textSize;
  uint32_t cycle;
  uint16_t pc;
  uint8_t a;
  uint8_t x;
  uint8_t y;
  uint8_t s;
  uint8_t p;
};
