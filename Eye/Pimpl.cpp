#include "pch.hpp"
#include "Pimpl.hpp"
#include "WinImgui.hpp"
#include "Ex.hpp"
#include "dat.h"
#include "fnt.h"

wchar_t gClassName[] = L"EyeWindowClass";

#define V_THROW(x) { HRESULT hr_ = (x); if( FAILED( hr_ ) ) { throw Ex{} << "DXError"; } }
#define L_ERROR std::cerr
#define L_DEBUG std::cout

LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{

  switch ( msg )
  {
  case WM_CREATE:
  {
    Pimpl * pEye = reinterpret_cast<Pimpl *>( reinterpret_cast<LPCREATESTRUCT>( lParam )->lpCreateParams );
    assert( pEye );
    try
    {
      pEye->initialize( hWnd );
    }
    catch ( std::exception const & ex )
    {
      MessageBoxA( nullptr, ex.what(), "Eye Error", MB_OK | MB_ICONERROR );
      PostQuitMessage( 0 );
    }
    SetWindowLongPtr( hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>( pEye ) );

    EnableMenuItem( GetSystemMenu( hWnd, FALSE ), SC_CLOSE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED );
    return 0;
  }
  case WM_CLOSE:
    DestroyWindow( hWnd );
    break;
  case WM_DESTROY:
    if ( Pimpl * pImpl = reinterpret_cast<Pimpl *>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) ) )
    {
      pImpl->quit();
    }
    SetWindowLong( hWnd, GWLP_USERDATA, NULL );
    break;
  default:
    if ( Pimpl * pImpl = reinterpret_cast<Pimpl *>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) ) )
    {
      if ( pImpl->win32_WndProcHandler( hWnd, msg, wParam, lParam ) )
        return true;
    }
    return DefWindowProc( hWnd, msg, wParam, lParam );
  }
  return 0;
}


Pimpl::~Pimpl()
{
  mDoLoop.store( false );
  if ( mWndProc.joinable() )
    mWndProc.join();
  if ( mRenderProc.joinable() )
    mRenderProc.join();
}

void Pimpl::initialize( HWND hWnd )
{
  mHWnd = hWnd;

  typedef HRESULT( WINAPI * LPD3D11CREATEDEVICE )( IDXGIAdapter *, D3D_DRIVER_TYPE, HMODULE, UINT32, CONST D3D_FEATURE_LEVEL *, UINT, UINT32, ID3D11Device **, D3D_FEATURE_LEVEL *, ID3D11DeviceContext ** );
  static LPD3D11CREATEDEVICE  s_DynamicD3D11CreateDevice = nullptr;
  HMODULE hModD3D11 = ::LoadLibrary( L"d3d11.dll" );
  if ( hModD3D11 == nullptr )
    throw std::runtime_error{ "DXError" };

  s_DynamicD3D11CreateDevice = (LPD3D11CREATEDEVICE)GetProcAddress( hModD3D11, "D3D11CreateDevice" );


  D3D_FEATURE_LEVEL  featureLevelsRequested = D3D_FEATURE_LEVEL_11_0;
  UINT               numFeatureLevelsRequested = 1;
  D3D_FEATURE_LEVEL  featureLevelsSupported;

  HRESULT hr = s_DynamicD3D11CreateDevice( nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr,
#ifndef NDEBUG
    D3D11_CREATE_DEVICE_DEBUG,
#else
    0,
#endif
    & featureLevelsRequested, numFeatureLevelsRequested, D3D11_SDK_VERSION, &mD3DDevice, &featureLevelsSupported, &mImmediateContext );

  V_THROW( hr );

  DXGI_SWAP_CHAIN_DESC sd{ { 0, 0, { 60, 1 }, DXGI_FORMAT_R8G8B8A8_UNORM }, { 1, 0 }, DXGI_USAGE_UNORDERED_ACCESS | DXGI_USAGE_RENDER_TARGET_OUTPUT, 2, mHWnd, TRUE, DXGI_SWAP_EFFECT_DISCARD, 0 };

  ATL::CComPtr<IDXGIDevice> pDXGIDevice;
  V_THROW( mD3DDevice->QueryInterface( __uuidof( IDXGIDevice ), (void **)&pDXGIDevice ) );
  ATL::CComPtr<IDXGIAdapter> pDXGIAdapter;
  V_THROW( pDXGIDevice->GetParent( __uuidof( IDXGIAdapter ), (void **)&pDXGIAdapter ) );
  ATL::CComPtr<IDXGIFactory> pIDXGIFactory;
  V_THROW( pDXGIAdapter->GetParent( __uuidof( IDXGIFactory ), (void **)&pIDXGIFactory ) );
  ATL::CComPtr<IDXGISwapChain> pSwapChain;
  V_THROW( pIDXGIFactory->CreateSwapChain( mD3DDevice, &sd, &pSwapChain ) );
  mSwapChain = std::move( pSwapChain );

  D3D11_BUFFER_DESC bd{ sizeof( CB ), D3D11_USAGE_DEFAULT, D3D11_BIND_CONSTANT_BUFFER };
  V_THROW( mD3DDevice->CreateBuffer( &bd, NULL, &mCBCB ) );

  mImgui.reset( new WinImgui{ mHWnd, mD3DDevice, mImmediateContext } );

  prepareHex();

  mRenderProc = std::thread{ [this]()
  {
    renderProc();
  } };
}

void Pimpl::copyHex( uint8_t const * src, uint8_t * dst, size_t width )
{
  for ( size_t y = 0; y < 8; ++y )
  {
    memcpy( dst + y * 16, src + y * font_dat_len / 8, width );
  }
}


void Pimpl::fillHex( uint8_t hex, uint8_t * buf )
{
  char buffer[3];
  sprintf( buffer, "%02x", hex );

  auto hi = fontDesc( (int)buffer[0] );
  auto lo = fontDesc( (int)buffer[1] );

  copyHex( font_dat + hi->x, buf + 8 - hi->xadvance, hi->width );
  copyHex( font_dat + lo->x, buf + 8, lo->width );
}

void Pimpl::prepareHex()
{
  std::array<D3D11_SUBRESOURCE_DATA, 256> initData;
  std::vector<uint8_t> buf;
  buf.resize( 16 * 8 * initData.size() );

  for ( size_t i = 0; i < initData.size(); ++i )
  {
    auto & ini = initData[i];
    ini.pSysMem = buf.data() + i * 16 * 8;
    fillHex( (uint8_t)i, ( uint8_t * )ini.pSysMem );
    ini.SysMemPitch = 16;
    ini.SysMemSlicePitch = 0;
  }

  D3D11_TEXTURE2D_DESC descsrc{ 16, 8, 1, initData.size(), DXGI_FORMAT_R8_UNORM, { 1, 0 }, D3D11_USAGE_IMMUTABLE, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
  V_THROW( mD3DDevice->CreateTexture2D( &descsrc, initData.data(), &mHex ) );
  V_THROW( mD3DDevice->CreateShaderResourceView( mHex, NULL, &mHexSRV.p ) );
}


void Pimpl::quit()
{
  mDoLoop.store( false );
}

bool Pimpl::win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
  return mImgui->win32_WndProcHandler( hWnd, msg, wParam, lParam );
}

void Pimpl::renderPreview()
{
  float phase = std::sinf( ( std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::high_resolution_clock::now().time_since_epoch() ).count() % 1000 ) / 1000.0f * 2.0f * 3.14159f ) * 0.5f + 0.5f;

  RECT r;
  ::GetClientRect( mHWnd, &r );

  if ( mEyeWidth != mEye.getRescaledWidth() )
  {
    mEyeWidth = mEye.getRescaledWidth();
    mEyeHeight = mEye.getRescaledHeight();

    {
      mSourceSRV.Release();
      mSource.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R8_UINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mSource ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mSource, NULL, &mSourceSRV.p ) );
    }

    {
      mFetchSRV.Release();
      mFetch.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R32_SINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mFetch ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mFetch, NULL, &mFetchSRV.p ) );
    }

    {
      mReadMikeySRV.Release();
      mReadMikey.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R32_SINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mReadMikey ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mReadMikey, NULL, &mReadMikeySRV.p ) );
    }

    {
      mWriteMikeySRV.Release();
      mWriteMikey.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R32_SINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mWriteMikey ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mWriteMikey, NULL, &mWriteMikeySRV.p ) );
    }

    {
      mReadSuzySRV.Release();
      mReadSuzy.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R32_SINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mReadSuzy ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mReadSuzy, NULL, &mReadSuzySRV.p ) );
    }

    {
      mAccessSRV.Release();
      mAccess.Release();

      D3D11_TEXTURE2D_DESC descsrc{ mEyeWidth, mEyeHeight, 1, 1, DXGI_FORMAT_R32_SINT, { 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0 };
      V_THROW( mD3DDevice->CreateTexture2D( &descsrc, nullptr, &mAccess ) );
      V_THROW( mD3DDevice->CreateShaderResourceView( mAccess, NULL, &mAccessSRV.p ) );
    }

    if ( !mRendererCS )
    {
      char EYEWIDTH[10];
      char EYEHEIGHT[10];

      sprintf( EYEWIDTH, "%d", mEyeWidth );
      sprintf( EYEHEIGHT, "%d", mEyeHeight );

      D3D_SHADER_MACRO shaderMacros[] = {
        "EYEWIDTH", EYEWIDTH,
        "EYEHEIGHT", EYEHEIGHT,
        NULL, NULL
      };

      ATL::CComPtr<ID3D10Blob> csBlob;
      ATL::CComPtr<ID3D10Blob> csErrBlob;

      char computeShader[] =
#include "render.csh"

        D3DCompile( computeShader, strlen( computeShader ), "ComputeShader", shaderMacros, NULL, "main", "cs_5_0", 0, 0, &csBlob.p, &csErrBlob.p );
      if ( !csBlob ) // NB: Pass ID3D10Blob* pErrorBlob to D3DCompile() to get error showing in (const char*)pErrorBlob->GetBufferPointer(). Make sure to Release() the blob!
      {
        OutputDebugStringA( (char const *)csErrBlob->GetBufferPointer() );
        throw Ex{} << "cs blob error";
      }

      mRendererCS.Release();
      V_THROW( mD3DDevice->CreateComputeShader( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), nullptr, &mRendererCS ) );

      ATL::CComPtr<ID3D10Blob> csDisAsm;
      V_THROW( D3DDisassemble( csBlob->GetBufferPointer(), csBlob->GetBufferSize(), D3D_DISASM_ENABLE_INSTRUCTION_OFFSET | D3D_DISASM_INSTRUCTION_ONLY, "Waldek", &csDisAsm.p ) );
      OutputDebugStringA( (char *)csDisAsm->GetBufferPointer() );
    }
  }


  mSX = (std::max)( 1, mWinWidth / ( mPreviewWidth * 2 ) );
  mSY = (std::max)( 1, mWinHeight / mPreviewHeight );
  int s = (std::min)( mSX, mSY );

  if ( mWinHeight != ( r.bottom - r.top ) || ( mWinWidth != r.right - r.left ) )
  {
    mWinHeight = r.bottom - r.top;
    mWinWidth = r.right - r.left;

    if ( mWinHeight == 0 || mWinWidth == 0 )
    {
      return;
    }

    mSX = (std::max)( 1, mWinWidth / ( mPreviewWidth * 2 ) );
    mSY = (std::max)( 1, mWinHeight / mPreviewHeight );
    s = (std::min)( mSX, mSY );

    mBackBufferUAV.Release();
    mBackBufferRTV.Release();

    mSwapChain->ResizeBuffers( 0, mWinWidth, mWinHeight, DXGI_FORMAT_UNKNOWN, 0 );

    ATL::CComPtr<ID3D11Texture2D> backBuffer;
    V_THROW( mSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID *)&backBuffer ) );
    V_THROW( mD3DDevice->CreateUnorderedAccessView( backBuffer, nullptr, &mBackBufferUAV ) );
    V_THROW( mD3DDevice->CreateRenderTargetView( backBuffer, nullptr, &mBackBufferRTV ) );

  }

  mImmediateContext->UpdateSubresource( mSource, 0, NULL, mData.data(), mEyeWidth*sizeof(uint8_t), 0 );
  mImmediateContext->UpdateSubresource( mFetch, 0, NULL, mLastFetch.data(), mEyeWidth * sizeof( int32_t ), 0 );
  mImmediateContext->UpdateSubresource( mReadMikey, 0, NULL, mLastReadMikey.data(), mEyeWidth * sizeof( int32_t ), 0 );
  mImmediateContext->UpdateSubresource( mWriteMikey, 0, NULL, mLastWriteMikey.data(), mEyeWidth * sizeof( int32_t ), 0 );
  mImmediateContext->UpdateSubresource( mReadSuzy, 0, NULL, mLastReadSuzy.data(), mEyeWidth * sizeof( int32_t ), 0 );
  mImmediateContext->UpdateSubresource( mAccess, 0, NULL, mLastAccess.data(), mEyeWidth * sizeof( int32_t ), 0 );

  auto mapping = mEye.computeMapping( r, 8 );

  uint32_t w = mapping.rightPos - mapping.leftPos;
  uint32_t h = mapping.bottomPos - mapping.topPos;
  uint32_t w16 = w / 16 + ( w % 16 == 0 ? 0 : 1 );
  uint32_t h16 = h / 16 + ( h % 16 == 0 ? 0 : 1 );
  

  mCB = {
      mapping.leftPos,
      mapping.topPos,
      mapping.leftOff,
      mapping.topOff,
      mapping.scaleX,
      mapping.scaleY,
      w, h,
      mapping.scaleX / 32,
      mapping.scaleY / 16,
      16.0f / (float)mapping.scaleX,
      8.0f / (float)mapping.scaleY,
      mCurrentTimestamp - 60,
      1.0f / 60.0f,
      mShowLastAccess
  };

  mImmediateContext->UpdateSubresource( mCBCB, 0, NULL, &mCB, 0, 0 );

  mImmediateContext->CSSetConstantBuffers( 0, 1, &mCBCB.p );
  std::array<ID3D11ShaderResourceView* const, 7> srv{ mHexSRV.p, mSourceSRV.p, mFetchSRV.p, mReadMikeySRV.p, mWriteMikeySRV.p, mReadSuzySRV.p, mAccessSRV.p };
  mImmediateContext->CSSetShaderResources( 0, 7, srv.data() );
  mImmediateContext->CSSetUnorderedAccessViews( 0, 1, &mBackBufferUAV.p, nullptr );
  mImmediateContext->CSSetShader( mRendererCS, nullptr, 0 );
  float v[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
  mImmediateContext->ClearUnorderedAccessViewFloat( mBackBufferUAV, v );

  if ( mapping.bottomPos > mapping.topPos )
    mImmediateContext->Dispatch( w16, h16, 1 );

  std::array<ID3D11UnorderedAccessView * const, 1> uav{};
  mImmediateContext->CSSetUnorderedAccessViews( 0, 1, uav.data(), nullptr );

  std::array<ID3D11ShaderResourceView * const, 7> srvc{};
  mImmediateContext->CSSetShaderResources( 0, 7, srvc.data() );

}

void Pimpl::eyeConfig( ImGuiIO & io )
{
  static std::optional<ImVec2> dragDelta = std::nullopt;

  ImGui::Begin( "Eye config" );
  bool hovered = ImGui::IsWindowHovered( ImGuiHoveredFlags_ChildWindows | ImGuiHoveredFlags_AllowWhenBlockedByPopup );

  BOOST_SCOPE_EXIT_ALL(&)
  {
    ImGui::End();
  };

  Point mousePos{ (int)io.MousePos.x, (int)io.MousePos.y };
  Point origin = mEye.getOrigin();
  auto scale = mEye.getScale();

  int w = mEye.getRescaledWidth() / 16;
  std::stringstream ss;
  ss << mEyeWidth << "x" << mEyeHeight;
  auto str = ss.str();
  ImGui::Checkbox( "Last Access", &mShowLastAccess ); ImGui::SameLine();
  if ( ImGui::Button( "Clear" ) )
  {
    std::fill( mLastAccess.begin(), mLastAccess.end(), 0 );
  }

  ImGui::SliderInt( "dimension", &w, 1, 16, str.c_str() );
  hovered |= ImGui::IsItemActive();
  mEye.updateWidth( w * 16 );

  if ( io.MouseWheel > 0 )
  {
    if ( io.KeyShift )
    {
      mEye.bigUpscale( 8 );
    }
    else
    {
      mEye.smallUpscale();
    }
  }
  else if ( io.MouseWheel < 0 )
  {
    if ( io.KeyShift )
    {
      mEye.bigDownscale( 8 );
    }
    else
    {
      mEye.smallDownscale();
    }
  }

  if ( !hovered )
  {
    if ( ImGui::IsMouseDragging( ImGuiMouseButton_Left, 1.0f ) )
    {
      dragDelta = ImGui::GetMouseDragDelta( ImGuiMouseButton_Left, 1.0f );
      mEye.drag( { (int)dragDelta->x, (int)dragDelta->y } );
    }
    else if ( dragDelta.has_value() )
    {
      mEye.endDrag();
    }
  }
}

void Pimpl::renderGui()
{
  ImGuiIO & io = ImGui::GetIO();

  //temporary
  if ( io.KeysDown[VK_RIGHT] )
  {
    mOffsetX = (std::min)( (int)mWorkspaceWidth / 4 - 40, mOffsetX + 1 );
  }
  else if ( io.KeysDown[VK_LEFT] )
  {
    mOffsetX = (std::max)( 0, mOffsetX - 1 );
  }
  else if ( io.KeysDown[VK_DOWN] )
  {
    mOffsetY = (std::min)( (int)mWorkspaceHeight / 8 - 24, mOffsetY + 1 );
  }
  else if ( io.KeysDown[VK_UP] )
  {
    mOffsetY = (std::max)( 0, mOffsetY - 1 );
  }

  mImgui->dx11_NewFrame();
  mImgui->win32_NewFrame();

  ImGui::NewFrame();

  //ImGui::ShowDemoWindow();

  eyeConfig( io );

  int x = ( (int)io.MousePos.x - mCB.destorgX + mCB.offsetX ) / mCB.scaleX;
  int y = ( (int)io.MousePos.y - mCB.destorgY + mCB.offsetY ) / mCB.scaleY;


  if ( x >= 0 && x < (int)mEyeWidth && y >= 0 && y < (int)mEyeHeight )
  {
    int adr = x + y * mEyeWidth;

    char buf[20];
    sprintf( buf, "%04x:%02x", adr, mData[adr] );
    ImGui::SetTooltip( buf );
  }

  ImGui::Render();

  mImmediateContext->OMSetRenderTargets( 1, &mBackBufferRTV.p, nullptr );
  mImgui->dx11_RenderDrawData( ImGui::GetDrawData() );

  std::array<ID3D11RenderTargetView * const, 1> rtv{};
  mImmediateContext->OMSetRenderTargets( 1, rtv.data(), nullptr );

  mSwapChain->Present( 1, 0 );
  if ( mAccessCount > 0 )
  {
    mCurrentTimestamp += 1;
    mAccessCount = 0;
  }
}

bool Pimpl::isWorking() const
{
  return mDoLoop.load();
}

void Pimpl::fetch( uint16_t adr )
{
  mLastFetch[adr] = mCurrentTimestamp;
  mLastAccess[adr] = mCurrentTimestamp;
  mAccessCount += 1;
}

void Pimpl::readMikey( uint16_t adr )
{
  mLastReadMikey[adr] = mCurrentTimestamp;
  mLastAccess[adr] = mCurrentTimestamp;
  mAccessCount += 1;
}

void Pimpl::writeMikey( uint16_t adr, uint8_t value )
{
  mData[adr] = value;
  mLastWriteMikey[adr] = mCurrentTimestamp;
  mAccessCount += 1;
}

void Pimpl::readSuzy( uint16_t adr )
{
  mLastReadSuzy[adr] = mCurrentTimestamp;
  mLastAccess[adr] = mCurrentTimestamp;
  mAccessCount += 1;
}

void Pimpl::writeSuzy( uint16_t adr, uint8_t value )
{
  mData[adr] = value;
}

void Pimpl::tick()
{
}

Pimpl::Pimpl() : mEye{}, mWinWidth{}, mWinHeight{}, mPreviewWidth{ 256 }, mPreviewHeight{ 256 }, mDoLoop{ true }, mEyeWidth{}, mAccessCount{}, mShowLastAccess{}
{
  constexpr size_t size = 256 * 256 + 224;  //224 bytes is addressable excess when the width of view is 240
  mData.resize( size );  
  mLastFetch.resize( size );
  mLastReadMikey.resize( size );
  mLastWriteMikey.resize( size );
  mLastReadSuzy.resize( size );
  mLastAccess.resize( size );
  mCurrentTimestamp = 0;



  mWndProc = std::thread{ [this]()
  {
    wndProc();
  } };
}

void Pimpl::wndProc()
{
  auto hInstance = GetModuleHandle( nullptr );

  WNDCLASSEX wc{ sizeof( WNDCLASSEX ), 0, WndProc, 0, 0, hInstance, nullptr, LoadCursor( nullptr, IDC_ARROW ), (HBRUSH)( COLOR_WINDOW + 1 ), nullptr, gClassName, nullptr };

  if ( !RegisterClassEx( &wc ) )
  {
    mDoLoop.store( false );
    return;
  }

  auto hwnd = CreateWindowEx( WS_EX_DLGMODALFRAME, gClassName, L"Eye", WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, 980, 619, nullptr, nullptr, hInstance, reinterpret_cast<LPVOID>( this ) );

  ShowWindow( hwnd, SW_SHOW );
  UpdateWindow( hwnd );

  MSG msg{};

  while ( isWorking() )
  {
    while ( PeekMessage( &msg, mHWnd, 0, 0, PM_REMOVE ) )
    {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
    }

    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
  }
}

void Pimpl::renderProc()
{
  while ( isWorking() )
  {
    renderPreview();
    renderGui();
  }
}
