#pragma once


class Pimpl;

class Eye
{
  std::shared_ptr<Pimpl> mPimpl;
public:

  Eye();
  void fetch( uint16_t adr );
  void readMikey( uint16_t adr );
  void writeMikey( uint16_t adr, uint8_t value );
  void readSuzy( uint16_t adr );
  void writeSuzy( uint16_t adr, uint8_t value );
  void tick();

  bool isWorking() const;

};
