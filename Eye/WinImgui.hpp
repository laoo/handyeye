#pragma once

class WinImgui
{
public:
  WinImgui( HWND hWnd, ATL::CComPtr<ID3D11Device> pD3DDevice, ATL::CComPtr<ID3D11DeviceContext> pDeviceContext );
  ~WinImgui();

  void win32_NewFrame();

  void     dx11_NewFrame();
  void     dx11_RenderDrawData( ImDrawData* draw_data );
  bool win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

private:
  void win32_UpdateMousePos();
  bool win32_UpdateMouseCursor();
  void dx11_InvalidateDeviceObjects();
  void dx11_CreateFontsTexture();
  void dx11_SetupRenderState( ImDrawData* draw_data, ID3D11DeviceContext* ctx );

private:

  HWND mhWnd;
  ATL::CComPtr<ID3D11Device>              md3dDevice;
  ATL::CComPtr<ID3D11DeviceContext>       md3dDeviceContext;

  INT64                mTime;
  INT64                mTicksPerSecond;
  ImGuiMouseCursor     mLastMouseCursor;

  int mVertexBufferSize;
  int mIndexBufferSize;

  struct VERTEX_CONSTANT_BUFFER
  {
    float   mvp[4][4];
  };

  ATL::CComPtr<ID3D11Buffer>              mVB;
  ATL::CComPtr<ID3D11Buffer>              mIB;
  ATL::CComPtr<ID3D10Blob>                mVertexShaderBlob;
  ATL::CComPtr<ID3D11VertexShader>        mVertexShader;
  ATL::CComPtr<ID3D11InputLayout>         mInputLayout;
  ATL::CComPtr<ID3D11Buffer>              mVertexConstantBuffer;
  ATL::CComPtr<ID3D10Blob>                mPixelShaderBlob;
  ATL::CComPtr<ID3D11PixelShader>         mPixelShader;
  ATL::CComPtr<ID3D11SamplerState>        mFontSampler;
  ATL::CComPtr<ID3D11ShaderResourceView>  mFontTextureView;
  ATL::CComPtr<ID3D11RasterizerState>     mRasterizerState;
  ATL::CComPtr<ID3D11BlendState>          mBlendState;
  ATL::CComPtr<ID3D11DepthStencilState>   mDepthStencilState;
};
