#include "pch.hpp"
#include "Eye.hpp"
#include "Pimpl.hpp"


Eye::Eye() : mPimpl{ std::make_shared<Pimpl>() }
{
}

void Eye::fetch( uint16_t adr )
{
  mPimpl->fetch( adr );
}

void Eye::readMikey( uint16_t adr )
{
  mPimpl->readMikey( adr );
}

void Eye::writeMikey( uint16_t adr, uint8_t value )
{
  mPimpl->writeMikey( adr , value );
}

void Eye::readSuzy( uint16_t adr )
{
  mPimpl->readSuzy( adr );
}

void Eye::writeSuzy( uint16_t adr, uint8_t value )
{
  mPimpl->writeSuzy( adr, value );
}

void Eye::tick()
{
  mPimpl->tick();
}

bool Eye::isWorking() const
{
  return mPimpl->isWorking();
}
