#pragma once

class WinImgui;

#include "EyeDesc.hpp"

class Pimpl
{
public:
  Pimpl();
  ~Pimpl();

  bool isWorking() const;

  void fetch( uint16_t adr );
  void readMikey( uint16_t adr );
  void writeMikey( uint16_t adr, uint8_t value );
  void readSuzy( uint16_t adr );
  void writeSuzy( uint16_t adr, uint8_t value );
  void tick();

private:

  struct CB
  {
    int32_t destorgX;
    int32_t destorgY;
    int32_t offsetX;
    int32_t offsetY;
    int32_t scaleX;
    int32_t scaleY;
    uint32_t groupsX;
    uint32_t groupsY;
    int32_t soffx;
    int32_t soffy;
    float sx16;
    float sx8;
    int32_t currentTick;
    float fadeOut;
    int32_t lastAccessMask;
    uint32_t fill3;
  };

private:

  friend LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
  void initialize( HWND hWnd );
  void quit();
  bool win32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
  void renderPreview();
  void renderGui();

  void wndProc();
  void renderProc();
  void eyeConfig( ImGuiIO & io );
  void prepareHex();
  void fillHex( uint8_t hex, uint8_t * buf );
  void copyHex( uint8_t const * src, uint8_t * dst, size_t width );

private:
  EyeDesc mEye;
  std::atomic_bool mDoLoop;
  std::atomic<HWND> mHWnd;
  std::thread mWndProc;
  std::thread mRenderProc;
  ATL::CComPtr<ID3D11Device>              mD3DDevice;
  ATL::CComPtr<ID3D11DeviceContext>       mImmediateContext;
  ATL::CComPtr<IDXGISwapChain>            mSwapChain;
  ATL::CComPtr<ID3D11ComputeShader>       mRendererCS;
  ATL::CComPtr<ID3D11Buffer>              mCBCB;
  ATL::CComPtr<ID3D11UnorderedAccessView> mBackBufferUAV;
  ATL::CComPtr<ID3D11RenderTargetView>    mBackBufferRTV;
  ATL::CComPtr<ID3D11Texture2D>           mHex;
  ATL::CComPtr<ID3D11ShaderResourceView>  mHexSRV;
  ATL::CComPtr<ID3D11Texture2D>           mSource;
  ATL::CComPtr<ID3D11ShaderResourceView>  mSourceSRV;
  ATL::CComPtr<ID3D11Texture2D>           mFetch;
  ATL::CComPtr<ID3D11ShaderResourceView>  mFetchSRV;
  ATL::CComPtr<ID3D11Texture2D>           mReadMikey;
  ATL::CComPtr<ID3D11ShaderResourceView>  mReadMikeySRV;
  ATL::CComPtr<ID3D11Texture2D>           mWriteMikey;
  ATL::CComPtr<ID3D11ShaderResourceView>  mWriteMikeySRV;
  ATL::CComPtr<ID3D11Texture2D>           mReadSuzy;
  ATL::CComPtr<ID3D11ShaderResourceView>  mReadSuzySRV;
  ATL::CComPtr<ID3D11Texture2D>           mAccess;
  ATL::CComPtr<ID3D11ShaderResourceView>  mAccessSRV;

  CB mCB;
  uint32_t mWorkspaceWidth;
  uint32_t mWorkspaceHeight;
  int mWinWidth;
  int mWinHeight;
  int mOffsetX;
  int mOffsetY;
  int mSX;
  int mSY;
  uint32_t mEyeWidth;
  uint32_t mEyeHeight;
  uint32_t mErrorMask;
  std::unique_ptr<WinImgui> mImgui;
  std::vector<uint8_t> mData;
  std::vector<uint32_t> mLastFetch;
  std::vector<uint32_t> mLastReadMikey;
  std::vector<uint32_t> mLastWriteMikey;
  std::vector<uint32_t> mLastReadSuzy;
  std::vector<uint32_t> mLastAccess;
  uint32_t mAccessCount;
  int32_t mCurrentTimestamp;
  bool mShowLastAccess;

  int mPreviewWidth;
  int mPreviewHeight;

};
