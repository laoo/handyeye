// EyeTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>
#include <thread>
#include "Eye.hpp"

int main()
{
  Eye eye{};

  while ( eye.isWorking() )
  {
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
  }
  return 0;
}

